# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.3] - 2020-03-09
### Changed
- Link fixes

## [1.0.2] - 2020-03-09
### Changed
- Updated README to mention the structure of the request and response objects in action parameters

## [1.0.1] - 2020-03-09
### Changed
- README fix

## [1.0.0] - 2020-03-09

### Added
- In Route 1.0.0

[Unreleased]: https://gitlab.com/jsonsonson/in-route/compare/v1.0.3...HEAD
[1.0.3]: https://gitlab.com/jsonsonson/in-route/compare/v1.0.2...v1.0.3
[1.0.2]: https://gitlab.com/jsonsonson/in-route/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/jsonsonson/in-route/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/jsonsonson/in-route/compare/v0.0.0...v1.0.0
