# [in-route](https://gitlab.com/jsonsonson/in-route.git)
[![license](https://raw.githubusercontent.com/jsonsonson/gitlab-badges/master/mit.svg?sanitize=true)](https://gitlab.com/jsonsonson/in-route/raw/master/LICENSE)
[![npm version](https://raw.githubusercontent.com/jsonsonson/gitlab-badges/master/npm-in-route.svg?sanitize=true)](https://www.npmjs.com/package/in-route)
[![vulnerabilities](https://snyk.io/test/npm/in-route/badge.svg)](https://snyk.io/test/npm/in-route)
[![downloads](https://img.shields.io/npm/dm/in-route.svg?style=flat)](https://npmjs.com/package/in-route)
[![build status](https://gitlab.com/jsonsonson/in-route/badges/v1.0.3/pipeline.svg)](https://gitlab.com/jsonsonson/in-route/pipelines)

A web route matching library built to read from a simplistic configuration

## Getting Started
`In Route` can be used as a simple front end router, such as for React applications and other approaches. Or, this package can be used in tandem with packages like the HTTP module or Express.js to create an API. It's fairly lightweight and effective at handling complex routes that may inlude paramters, queries, etc.

## Installation
`npm install in-route`

## Features
- Mapping for all HTTP methods
- Automatic parsing of parameters, query strings, and path
- Custom variable object passed along with each route
- Case sensitive and case insensitive routes
- Ready to plug in with Node's HTTP module or the Express module

## API
- [Router](#routerroutes-config)
- [handle](#handleuri-method-body-headers-httpobject)
---

### *Router(routes[, config])*
Router constructor.
#### Parameters
- `routes` (Object) [Routes object.](#example-routes-object)
- `config` (Object) Config object.
  - `caseSensitive` (Boolean) True if routes should be handled with case sensitivity, otherwise false. Defaults to true.
  - `defaultAction` (Function) Default action to execute if a route is not handled. Defaults to an empty function.
---

### *handle(uri[, method[, body[, headers[, httpObject]]]])*
Parses a route and executes the action tied to the route, passing request and
response objects to the action.
#### Parameters
- `uri` (String) URI to handle and parse
- `method` (String) HTTP method in uppercase. Defaults to `GET`
- `body` (Object) Request body object passed from HTTP library.
- `headers` (Object) Headers object passed from HTTP library.
- `httpObject` (Object) Object containing HTTP library's request and response object's structured as `{ req: <requestObject>, res: <responseObject> }`

#### Examples
```javascript
const Router = require('in-route');
const routes = require('path-to-routes-export');

const router = new Router(routes, {
  caseSensitive: true,
  defaultAction: (req, res) => { console.log('default', { req, res }); },
});

router.handle('http://www.example.uri/param1?query1=true', 'GET', bodyObj, headersObj, { req: reqObj, res: resObj });
```
---

## Example Routes object
```javascript
/*
Request object:
{
  vars: Object,
  body: Object,
  headers: Object,
  query: Object,
  params: Object,
  url: String,
  path: String,
  method: String,
  extraPath: String,
  httpRequest: Object, // HTTP module or Express request object
}

The response object is the response from the HTTP module/Express
*/
module.exports = {
  'GET /': {
    action: (req, res) => { return res.send({ query: req.query, params: req.params, vars: req.vars, body: req.body }); },
    vars: 'home',
  },
  'post /hi': {
    action: (req, res) => { return res.send({ query: req.query, params: req.params, vars: req.vars, body: req.body }); },
  },
  'GET /hi/': {
    action: (req, res) => { return res.send({ query: req.query, params: req.params, vars: req.vars, body: req.body }); },
    vars: {},
  },
  'GET /hi/{hello_id}': {
    action: (req, res) => { return res.send({ query: req.query, params: req.params, vars: req.vars, body: req.body }); },
    vars: {
      hasVar: true,
    },
  },
  // Method keyword is optional. Defaults to GET
  'hi/{hello_id}/yEs': {
    action: (req, res) => { return res.send({ query: req.query, params: req.params, vars: req.vars, body: req.body }); },
    vars: {
      thing: 'Yes this is a thing',
      pizza: ['pep', 'chez', '!pineapple'],
    },
  },
  'delete /hi/{hello_id}/YES/{yes_id}/': {
    action: (req, res) => { return res.send({ query: req.query, params: req.params, vars: req.vars, body: req.body }); },
  },
  'get /hi/{hello_id}/YES/{yes_id}/': {
    action: (req, res) => { return res.send({ query: req.query, params: req.params, vars: req.vars, body: req.body }); },
    vars: {
      crazy: true,
    }
  }
};
```

## Contributing
Please read [CONTRIBUTING.md](https://gitlab.com/jsonsonson/in-route/blob/master/CONTRIBUTING.md) before submitting issues and merge requests.

## License
MIT
