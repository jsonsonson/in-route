const prompt = require('prompt');
const fs = require('fs');
const { spawnSync } = require('child_process');

const todos = [];
prompt.message = ' ';

console.log('Make sure you added all the files to be committed before continuing');
console.log();

prompt.start();
prompt.get([
  'branch',
  'commit message',
  'new version?',
], (err, result) => {
  if (!err) {
    let newVersion = result['new version?'].toLowerCase();
    newVersion = newVersion === 'true' || newVersion.charAt(0) === 'y';

    if (newVersion && result['branch'].toLowerCase() === 'master') {
      prompt.get([
        'version update type (major, minor, patch)',
        'additions',
        'changes',
        'removals',
      ], (err2, versResult) => {
        if (!err2) {
          const versionType = versResult['version update type (major, minor, patch)'];
          let vers = `v${updateVersion(versionType)}`;

          let added = versResult['additions'].trim();
          let changes = versResult['changes'].trim();
          let removed = versResult['removals'].trim();

          if (added.length > 0) {
            added = added.split(',');
          }

          if (changes.length > 0) {
            changes = changes.split(',');
          }

          if (removed.length > 0) {
            removed = removed.split(',');
          }

          const date = new Date().toLocaleDateString([], { year: 'numeric', month: '2-digit', day: '2-digit' });

          updateChangelog(vers.substr(1), added, changes, removed, date);
          updateBadges(vers.substr(1));
          updateReadmeBadges(vers.substr(1));

          spawnSync('git', ['commit', '-m', `${vers}: ${result['commit message']}`]);
          spawnSync('git', ['tag', '-a', `${vers}`, '-m', `${result['commit message']}`]);
          console.log(`Created tag ${vers}`);
          console.log('Pushing changes...');
          spawnSync('git', ['push', 'origin', `${result['branch']}`, '--follow-tags']);
          console.log('Push complete');
        } else {
          console.log();
          process.exit(1);
        }
      });
    } else {
      updateBadges();
      updateReadmeBadges();
      spawnSync('git', ['commit', '-m', `${result['commit message']}`]);
      console.log('Pushing changes...');
      spawnSync('git', ['push', 'origin', `${result['branch']}`, '--follow-tags']);
      console.log('Push complete');
    }
  } else {
    console.log();
    process.exit(1);
  }
});

function updateVersion(updateType) {
  console.log('Updating version...');
  const upType = updateType.trim().toLowerCase();

  const packageJson = require('./package.json');
  let version = packageJson.version;
  version = version.split('.');

  let major = Number(version[0]);
  let minor = Number(version[1]);
  let patch = Number(version[2]);

  switch (upType) {
    case 'patch':
      patch++;
      break;
    case 'minor':
      minor++;
      break;
    case 'major':
      patch = 0;
      minor = 0;
      major++;
      break;
    default:
      console.log('Enter a valid version update type');
      process.exit(1);
  }

  version = `${major}.${minor}.${patch}`;
  packageJson.version = version;

  fs.writeFileSync('package.json', JSON.stringify(packageJson, null, '  '));
  spawnSync('npm', ['install']);
  spawnSync('git', ['add', 'package.json', 'package-lock.json']);
  console.log(`Version updated to ${version}`);

  return version;
}

function updateChangelog(version, added, changes, removed, date) {
  const versions = require('./versions.json');
  const previous = versions.current;
  versions.current = version;
  versions.versions.Unreleased.link = versions.versions.Unreleased.link.replace(previous, version);
  versions.versions[version] = {
    link: `https://gitlab.com/jsonsonson/in-route/compare/v${previous}...v${version}`,
    changed: changes || [],
    added: added || [],
    removed: removed || [],
    date,
  }

  let changelog = '# Changelog\n';
  changelog += 'All notable changes to this project will be documented in this file.\n\n';
  changelog += 'The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)\n';
  changelog += 'and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).\n\n';

  const versionsArr = versions.versions;
  const versionKeys = Object.keys(versionsArr);
  const links = [];
  const unreleased = versionsArr['Unreleased'];

  changelog += '## [Unreleased]\n\n';
  links.push({
    vers: 'Unreleased',
    link: unreleased.link,
  })

  versionKeys.reverse().forEach((vers) => {
    if (vers !== 'Unreleased') {
      const v = versionsArr[vers];
      const link = v.link;
      const added = v.added;
      const changed = v.changed;
      const removed = v.removed;
      const date = v.date;

      if (link) {
        changelog += `## [${vers.trim()}]${date ? ' - ' + date.trim(): ''}\n`;
        links.push({
          vers,
          link,
        });
      } else {
        changelog += `## ${vers.trim()}${date ? ' - ' + date.trim(): ''}\n`;
      }

      if (v.changed && v.changed.length) {
        changelog += '### Changed\n';

        v.changed.forEach((change) => {
          const c = change.trim();

          if (c.length > 0) {
            changelog += `- ${change.trim()}\n`;
          }
        });
      }

      if (v.added && v.added.length) {
        changelog += '\n### Added\n';

        v.added.forEach((add) => {
          const a = add.trim();

          if (a.length > 0) {
            changelog += `- ${add.trim()}\n`;
          }
        });
      }

      if (v.removed && v.removed.length) {
        changelog += '\n### Removed\n';

        v.removed.forEach((remove) => {
          const r = remove.trim();

          if (r.length > 0) {
            changelog += `- ${remove.trim()}\n`;
          }
        });
      }

      changelog += `\n`;
    }
  });

  if (links.length) {
    links.forEach((link) => {
      changelog += `[${link.vers.trim()}]: ${link.link.trim()}\n`;
    });
  }

  fs.writeFileSync('versions.json', JSON.stringify(versions, null, '  '));
  fs.writeFileSync('CHANGELOG.md', changelog);
  spawnSync('git', ['add', 'CHANGELOG.md', 'versions.json']);
}

function updateBadges(newVersion) {
  const update = () => {
    if (newVersion !== undefined) {
      const versStr = `${newVersion}`;
      let npmSVG = fs.readFileSync('badges/npm-in-route.svg').toString();
      npmSVG = npmSVG.replace(/textLength="\d+">\d+\.\d+\.\d+/g, `textLength="${versStr.length * 60}">${versStr}`);
      fs.writeFileSync('badges/npm-in-route.svg', npmSVG);
    }

    // TODO put updates to other badges here (if there will be any)

    process.chdir('badges/');
    spawnSync('git', ['add', '.']);
    spawnSync('git', ['commit', '-m', 'Badge updates']);
    spawnSync('git', ['push', 'origin', 'master']);
    process.chdir('../');
  };

  if (!fs.existsSync('badges')) {
    spawnSync('git', ['clone', 'https://github.com/jsonsonson/gitlab-badges.git', 'badges']);
  }

  update();
}

function updateReadmeBadges(newVersion) {
  if (newVersion !== undefined) {
    let readme = fs.readFileSync('README.md').toString();
    readme = readme
      .replace(/https:\/\/gitlab.com\/jsonsonson\/in-route\/badges\/v\d+\.\d+\.\d+/g, `https://gitlab.com/jsonsonson/in-route/badges/v${newVersion}`)
      .replace(/https:\/\/gitlab.com\/jsonsonson\/in-route\/commits\/v\d+\.\d+\.\d+/g, `https://gitlab.com/jsonsonson/in-route/commits/v${newVersion}`);
    fs.writeFileSync('README.md', readme);
  }

  spawnSync('git', ['add', 'README.md']);
}
