const args = require('in-command');
const url = require('url');

function removeTrailingSlash(routeStr) {
  if (routeStr.length > 1 && routeStr.slice(-1) === '/') {
    return routeStr.slice(0, -1);
  }

  return routeStr;
}

function addLeadingSlash(routeStr) {
  if (routeStr.charAt(0) !== '/') {
    return `/${routeStr}`;
  }

  return routeStr;
}

function formatRoutes(routesObj, config) {
  const newRoutesArr = [];
  Object.keys(routesObj).forEach(route => {
    let newRouteStr = `${route}`.trim();

    // Replace any consecutive slashes with a single slash, and consecutive spaces
    // with a single space
    newRouteStr = newRouteStr
      .replace(/\/{2,}/g, '/')
      .replace(/\s{2,}/g, ' ');

    // Split method and route
    let method = 'GET';
    const newRouteStrSplit = newRouteStr.split(' ');

    if (newRouteStrSplit.length && newRouteStrSplit.length === 1) {
      // If the only thing provided is a method, default to base route
      if (['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH'].includes(`${newRouteStrSplit[0]}`.trim().toUpperCase())) {
        method = `${newRouteStrSplit[0]}`.trim();
        newRouteStr = '/';
      } else {
        // Is a route but given no method - default to GET
        method = 'GET';
        newRouteStr = `${newRouteStrSplit[0]}`.trim();
      }
    } else {
      method = `${newRouteStrSplit[0]}`.trim().toUpperCase();
      newRouteStr = `${newRouteStrSplit[1]}`.trim();
    }

    // Add trailing and leading slash
    newRouteStr = addLeadingSlash(newRouteStr);
    newRouteStr = removeTrailingSlash(newRouteStr);
    // If not case sensitive, lowercase route
    if (!config.caseSensitive) {
      newRouteStr = newRouteStr.toLowerCase();
    }

    newRoutesArr.push({
      ...routesObj[route],
      method,
      route: newRouteStr,
    });
  });

  return newRoutesArr;
}

function buildRouteParser(routesArr, router) {
  const argParser = args.argParser();
  const getParser = args.argParser()
    .parseOnExit(false)
    .disableGeneratedHelp();
  const postParser = args.argParser()
    .parseOnExit(false)
    .disableGeneratedHelp();
  const deleteParser = args.argParser()
    .parseOnExit(false)
    .disableGeneratedHelp();
  const putParser = args.argParser()
    .parseOnExit(false)
    .disableGeneratedHelp();
  const headParser = args.argParser()
    .parseOnExit(false)
    .disableGeneratedHelp();
  const connectParser = args.argParser()
    .parseOnExit(false)
    .disableGeneratedHelp();
  const optionsParser = args.argParser()
    .parseOnExit(false)
    .disableGeneratedHelp();
  const traceParser = args.argParser()
    .parseOnExit(false)
    .disableGeneratedHelp();
  const patchParser = args.argParser()
    .parseOnExit(false)
    .disableGeneratedHelp();

  routesArr.reverse().forEach(routeObj => {
    const method = routeObj.method;

    let route = `${routeObj.route}`;
    route = route.replace(/{.*?}/g, '(.*)');
    route = new RegExp(route, 'g');

    const routeParser = (/*options, parameters, command*/) => {
      const vars = routeObj.vars || {};
      const params = {};
      let extraPath = '';

      let path = `${argParser.routeResults.path}`;

      // Remove extra path
      const normalPathCount = routeObj.route.split('/').length;
      let pathSplit = path.split('/');
      if (pathSplit.length > normalPathCount) {
        extraPath = `/${pathSplit.splice(normalPathCount, pathSplit.length - normalPathCount).join('/')}`;
      }
      path = pathSplit.join('/');

      let regexParamValues = route.exec(path);
      const paramValues = regexParamValues.slice(1);

      if (paramValues.length) {
        const regexParamKeys = /{(.*?)}/g;
        let paramKeys = regexParamKeys.exec(routeObj.route);
        let paramInd = 0;

        while (paramKeys !== null) {
          params[paramKeys[1]] = paramValues[paramInd++];
          paramKeys = regexParamKeys.exec(routeObj.route);
        }
      }

      const result = {
        ...argParser.routeResults,
        vars,
        params,
        extraPath,
      };

      let returnValue = {};

      if (routeObj.action && routeObj.action.constructor === Function) {
        const httpObject = argParser.httpObject;
        returnValue = routeObj.action({ ...result, httpRequest: httpObject.req || {} }, httpObject.res || {});
        router.returnValue = returnValue || {};
      }

      router.handled = true;
      delete argParser.routeResults;
      delete argParser.httpObject;
    };

    switch (method) {
      case 'GET':
        getParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
      case 'POST':
        postParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
      case 'PUT':
        putParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
      case 'DELETE':
        deleteParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
      case 'HEAD':
        headParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
      case 'CONNECT':
        connectParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
      case 'OPTIONS':
        optionsParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
      case 'TRACE':
        traceParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
      case 'PATCH':
        patchParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
      default:
        getParser.command(route, `${routeObj.method} ${routeObj.route}`, routeParser);
        break;
    }
  });

  argParser
    .command('GET', 'get method', getParser)
    .command('POST', 'post method', postParser)
    .command('DELETE', 'delete method', deleteParser)
    .command('PUT', 'put method', putParser)
    .command('HEAD', 'head method', headParser)
    .command('CONNECT', 'connect method', connectParser)
    .command('OPTIONS', 'options method', optionsParser)
    .command('TRACE', 'trace method', traceParser)
    .command('PATCH', 'patch method', patchParser)
    .parseOnExit(false)
    .disableGeneratedHelp();

  return argParser;
}

class Router {

  constructor(routes = {}, config = {
    caseSensitive: false,
    defaultAction: () => {},
  }) {
    this.handled = false;
    this.routes = formatRoutes(routes, config);
    this.parser = buildRouteParser(this.routes, this);
    this.defaultAction = config.defaultAction || (() => {});
    this.returnValue = null;
  }

  handle(uri = '', method = 'GET', body = {}, headers = {}, httpObject = { req: {}, res: {} }) {
    this.handled = false;
    this.returnValue = null;

    if (uri) {
      const _url = url.parse(uri);

      const query = {};
      const urlQuery = _url.query || '';

      if (urlQuery) {
        const urlQuerySplit = urlQuery.split('&');

        if (urlQuerySplit.length) {
          urlQuerySplit.forEach(queryItem => {
            if (queryItem) {
              const queryItemSplit = queryItem.split('=');
              query[queryItemSplit[0]] = decodeURIComponent(queryItemSplit[1]);
            }
          });
        }
      }

      if (_url) {
        const path = _url.pathname;
        const requestObj = {
          vars: {},
          body,
          headers,
          query,
          params: {},
          url: uri,
          path,
          method,
          extraPath: '',
        };

        this.parser.routeResults = requestObj;
        this.parser.httpObject = httpObject;
        this.parser.parse(['node', __filename, method, path]);

        if (!this.handled) {
          this.returnValue = this.defaultAction({ ...requestObj, httpRequest: httpObject.req || {} }, httpObject.res || {});
        }

        return this.returnValue;
      }
    }

    return null;
  }
}

module.exports = Router;
